/*
 * Config
 */
provider "aws" {}

/*
 * Variables
 */
variable "environment" {
  default = "staging"
}

variable "db_username" {}
variable "db_password" {}
variable "smtp_host" {}
variable "smtp_port" {}
variable "smtp_user" {}
variable "smtp_pass" {}

/*
 * Remote State
 */
terraform {
  backend "s3" {
    bucket = "ahwaa-terraform"
    region = "us-east-1"

    // This is the state key, please make sure this namespace we are using matches the environment file in line 10
    key = "staging/terraform.tfstate"
  }
}

/*
 * Resources
 */
module "vpc" {
  source = "./modules/network/vpc"

  environment = "${var.environment}"
}

// Database
resource "aws_db_subnet_group" "postgres_sg" {
  name        = "postgres-${var.environment}-sg"
  description = "postgres-${var.environment} RDS subnet group"
  subnet_ids  = ["${module.vpc.private_subnet_ids}"]
}

resource "aws_db_instance" "postgres" {
  identifier        = "postgres-${var.environment}"
  allocated_storage = "20"
  engine            = "postgres"
  engine_version    = "9.6.6"
  instance_class    = "db.t2.micro"
  name              = "ahwaa_${var.environment}"
  username          = "${var.db_username}"
  password          = "${var.db_password}"

  backup_window           = "22:00-23:59"
  maintenance_window      = "sat:20:00-sat:21:00"
  backup_retention_period = "7"

  vpc_security_group_ids = ["${module.vpc.rds_security_group_id}"]

  db_subnet_group_name = "${aws_db_subnet_group.postgres_sg.name}"
  parameter_group_name = "default.postgres9.6"

  multi_az                  = true
  storage_type              = "gp2"
  final_snapshot_identifier = "postgres-${var.environment}-final"

  tags {
    Terraform   = true
    Name        = "postgres-${var.environment}"
    Environment = "${var.environment}"
  }

  lifecycle {
    prevent_destroy = true
  }
}

// Key pair to be used in our servers
resource "aws_key_pair" "ahwaa" {
  key_name   = "ahwaa-ssh"
  public_key = ""

  lifecycle {
    prevent_destroy = true
  }
}

// Discourse instance
module "discourse" {
  source      = "./modules/compute/services/discourse"
  environment = "${var.environment}"

  discourse_hostname = "ahwaa.orlandodelaguila.com"

  discourse_smtp_address   = "${var.smtp_host}"
  discourse_smtp_user_name = "${var.smtp_user}"
  discourse_smtp_password  = "${var.smtp_pass}"

  discourse_db_host     = "${aws_db_instance.postgres.address}"
  discourse_db_name     = "ahwaa_${var.environment}"
  discourse_db_username = "${var.db_username}"
  discourse_db_password = "${var.db_password}"

  key_name        = "${aws_key_pair.ahwaa.key_name}"
  subnet_id       = "${element(module.vpc.public_subnet_ids, 0)}"
  security_groups = "${module.vpc.ec2_security_group_id}"
}

/*
 * Outputs
 */
output "public_ip" {
  value = "${module.discourse.public_ip}"
}
